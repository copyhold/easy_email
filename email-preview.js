jQuery(function($) {
  var clone = $('#edit-over-fixed').clone()
  clone.attr({type:'number',min: -3})
  $('#edit-over-fixed').replaceWith(clone)
  var tds = $('#nodes-preview td.views-field-field-diamond-rapoff')
  var minrap = tds.map(function(k,v) { return parseFloat(v.innerText)}).toArray().reduce(function(v1,v2) { return v1 + v2 },0) - 3
  minrap /= tds.length;
  clone = $('#edit-over-percent').clone()
  clone.attr({ type: 'number', min: Math.floor(minrap) })
  $('#edit-over-percent').replaceWith(clone);

  $('#updatepreview').click(function() {
    $('#nodes-preview').html('loading...')
    updatepreview();  
  });
  function updatepreview() {
  $.post(
    Drupal.settings.basePath + 'views/ajax', {
      view_name: 'email_product_list',
      view_name: 'sendemail2',
      view_display_id: 'block_1',
      view_display_id: 'block',
      view_args: [Drupal.settings.nids,$('#edit-client').val(), $('#edit-selectpricing-noprice').is(':checked'), $('#edit-over-fixed').val(), $('#edit-over-percent').val(),$('[name=selectpricing]:checked').val()].join('/')
      }, function(r) { 
      if (typeof r[1]!=='undefined') {
        $('#nodes-preview').html(r[1].data)
        $('#edit-emailtext').val(r[1].data)
      }
    })
  }
})

function preview_email(form, fs) {
  console.log(form);
}
