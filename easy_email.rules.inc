<?php
/**
 * Implement hook_rules_action_info().
 */
function easy_email_rules_action_info() {
  return array(
    'easy_email_rules_action_unsubscribe_all' => array(
      'label' => t('Unsubscribe all from a newsletter'),
      'group' => t('Simplenews'),
      'parameter' => array(
        'tid' => array(
          'type' => 'integer',
          'label' => t('Simplenews category'),
          'descrption' => t('For which newsletter category the unsubscription should happen.'),
          'options list' => 'simplenews_category_list',
        ),
      ),
    ),
  );
}
/**
 * Action Implementation: Unsubscribe an e-mail adress to a Simplenews newsletter.
 */
function easy_email_rules_action_unsubscribe_all($args, $settings) {
  // Pass the call forward.
  db_query('delete from {simplenews_subscription} where tid=:tid', array(':tid'=>$args));
}

